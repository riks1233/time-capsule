# Time Capsule #

### Repositooriumi tutvustus ###

* Veebilehe "Time Capsule" eesmärk on anda kasutajatele võimalus sisestada mingisuguseid andmeid või meeldetuletusi, mille saaksid nad kätte alles siis, kui (nende enda poolt seatud) tähtaeg ja kellaaeg kätte jõuab.
* Projekt on loodud järgneva õppeaine raames: [Veebirakenduste loomine (MTAT.03.230)](https://courses.cs.ut.ee/2017/vl/spring)
* Versioon 0.0.0
* [Projekti Wiki](https://bitbucket.org/riks1233/time-capsule/wiki/Home)
* [Prototüüp](https://bitbucket.org/riks1233/time-capsule/wiki/Protot%C3%BC%C3%BCp)
* [Projektiplaan](https://bitbucket.org/riks1233/time-capsule/wiki/Projektiplaan)
* [Etapp 2](https://bitbucket.org/riks1233/time-capsule/wiki/Etapp%202)
* [Etapp 3](https://bitbucket.org/riks1233/time-capsule/wiki/Etapp%203)
* [Etapp 4](https://bitbucket.org/riks1233/time-capsule/wiki/Etapp%204)
* [Etapp 5](https://bitbucket.org/riks1233/time-capsule/wiki/Etapp%205)
* [Etapp 6](https://bitbucket.org/riks1233/time-capsule/wiki/Etapp%206)
* [Etapp 7](https://bitbucket.org/riks1233/time-capsule/wiki/Etapp%207)
* [ID-kaart](https://bitbucket.org/riks1233/time-capsule/wiki/ID-kaart)

### Millest alustada, kui soov omal käel testida ###

* [Time Capsule koduleht](http://www.ajakapsel.me)
* Ei ole veel midagi testida.

### Autorid, kontaktandmed ###

* Silver Kirotar, kontakt: silver.kirotar@hotmail.com
* Richardas Keršis, kontakt: richardas.kersis@gmail.com
* Anton Tšugunov, kontakt: anton.tsugunov@gmail.com