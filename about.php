<?php
include_once 'lang/language.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_ABOUT'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<?php include_once 'config/google-config.php'; ?>
		<script async src="scripts/googleMapAPI.js" ></script>
	</head>
	<body>
	
	<div id="mainContainer">
		<div id="pageContent">
			<span><?= getLocalised('FIND'); ?></span>
			
			<div id="googleMap"></div>
			<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API; ?>&callback=myMap"></script>
			
			<br>
			<span><?= getLocalised('CONTACT_INFO'); ?>:</span>
			<div id="contact_info"></div>
			<br>
			<span><?= getLocalised('REPO'); ?>:</span>
			<div id="repo_link"></div>
			<script src="scripts/xmlLoader.js" ></script>
			<script src="scripts/about.js" ></script>
			
			<br><br>
			<form action="donate.php">
				<input type="submit" class="small_button" value="<?= getLocalised('MK_DONATION'); ?>">
			</form>
			<br><br>
			<a href="index.php" class="howto"><?= getLocalised('BACK_TO_MAIN'); ?></a>
		</div>
	</div>
	</body>

</html>