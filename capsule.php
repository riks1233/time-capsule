<?php 
include_once 'lang/language.php'; 
include_once 'config/google-config.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_CAPSULE'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<?php include 'logic/loginCheck.php'; ?>
		<script src="scripts/timer.js"></script>
		<!-- Google API -->
		<meta name="google-signin-client_id" content="<?=CLIENT_ID?>">
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<!-- End of Google API-->
		<script src ="downloaded/jquery/jquery-3.1.1.min.js"></script><!-- google log out jaoks-->
		<script src ="scripts/lateLoader.js"></script>
	</head>
	<body>

	<div id="topBar">
			<table id="top_menu">
				<tr>
					<td id="icon_box"><a href="user.php" data-logo-url="images/tcLogo.png"></a></td>
					<td id="name"><span><?php echo getName($mysqli, $_SESSION['username']);?></span></td>
					<td id="logout">
						<form id="logout_button" action="logic/logout.php"><button class="buttonInverted"><?= getLocalised('LOGOUT'); ?></button></form>
						<script src="scripts/googleAPI.js"></script><!-- google log out jaoks-->
						<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script><!-- google log out jaoks-->
					</td>
				</tr>
			</table>
	</div>
	<?php $id = (isset($_GET['id']) ? $_GET['id'] : '<NO_ID>');	?>
	<div id="mainContainer">
		<div id="pageContent">
			<p><?php 
				if ($id == '<NO_ID>') echo getLocalised('NO_ID'); 
				else echo getLocalised('CAPSULE') . $id; 
				getCapsuleTime($mysqli, $id);
			?></p>
			<br>
			<div id="closed_capsule">
				<div id="closed_capsule_content">
					<div id="timeBox">
						<span id="timeText"></span>
					</div>
				</div>
			</div>
			
			<br><br>
			<form method="post" action="capsuleOpening.php">
			<input type="hidden" name="id" value="<?= $id ?>"><br>	
			<button class="small_button" type="submit"><?= getLocalised('OPEN'); ?></Button>
			</form>
		</div>
	</div>
	<?php
		//$time = 1549777165000; //get time from db
		//call the timer($time) function
		$time;
		if ($id != '<NO_ID>') 
			$time = getCapsuleTime($mysqli, $id);
		else return;
		if ($time == null) return;
		if ($time < 0) $time = 0;
		echo "
		<script>
		timer(" . ($time * 1000) . ");
		</script>";
	?>
	</body>
</html>