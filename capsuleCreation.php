<?php 
include_once 'lang/language.php'; 
include_once 'config/google-config.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_CREATION'); ?></title>
		<script src="downloaded/jquery/jquery-3.1.1.min.js"></script> <!-- google log out ja combodate jaoks-->
		<script src="downloaded/combodate/combodate.js"></script> 
        <script src="downloaded/momentjs/moment.js"></script> 
		<script src="scripts/capsuleCreationJS.js"></script>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<?php include 'logic/loginCheck.php'; ?>
		<!-- Google API -->
		<meta name="google-signin-client_id" content="<?=CLIENT_ID?>">
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<!-- End of Google API-->
		<script src="scripts/tutorial_capsuleCreation.js"></script>
		<script src ="scripts/lateLoader.js"></script>

	</head>
	<body>
	<div id="topBar">
			<table id="top_menu">
				<tr>
					<td id="icon_box"><a href="user.php" data-logo-url="images/tcLogo.png"></a></td>
					<td id="name"><span><?php echo getName($mysqli, $_SESSION['username']);?></span></td>
					<td id="logout">
						<form id="logout_button" action="logic/logout.php"><button class="buttonInverted"><?= getLocalised('LOGOUT'); ?></button></form>
						<script src="scripts/googleAPI.js"></script><!-- google log out jaoks-->
						<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script><!-- google log out jaoks-->
					</td>
				</tr>
			</table>
	</div>
	<div id="mainContainer">
		<div id="pageContent">
			<p><?= getLocalised('CREATE'); ?></p>
			<br>
			<form action="logic/create.php" method="POST"><!-- POST method because messages are sensitive data :) -->
				<textarea title="<?= getLocalised('CREATE_MESSAGE_TITLE'); ?>" name="message" id="capsuleCreationText" maxlength="2048" rows="10" cols="50" placeholder="<?= getLocalised('MESSAGE'); ?>." required><?= isset($_SESSION['message']) ? validateInput($_SESSION['message']) : null ?></textarea><br>
				<p><?= getLocalised('CREATE_OPEN'); ?></p>
				<p>

				<input title="<?= getLocalised('CREATE_OPEN_TITLE'); ?>" name="datetime" id="dateTime" value="<?= isset($_SESSION['datetime']) ? validateInput($_SESSION['datetime']) : "2017-04-30 13:37" ?>" data-format="YYYY-MM-DD HH:mm" data-template="YYYY MMM D H : mm"> 

				<!-- Script moved to scripts/capsuleCreationJS.js (look in the head tag) -->
				</p>
				<div class="errorField"><?php
					if (isset($_GET['err_datetime'])){
						if ($_GET['err_datetime'] == 1) {
							echo getLocalised('CREATE_ERR_ONE');
						} elseif ($_GET['err_datetime'] == 0) {
							echo getLocalised('CREATE_ERR_NULL');
						}
					}
				?></div>
				<br>
				<button id="encloseBtn" class="small_button" type="submit"><?= getLocalised('CREATE_ENCLOSE'); ?></button>
			</form>
			<br>
			<script>drawNormalHelp("<?= getLocalised('TUTORIAL'); ?>")</script>
			<noscript>
			<p><a class="howto" href="tutorial.php"><?= getLocalised('TUTORIAL'); ?></a></p>
			</noscript>
		</div>
	</div>
		<div id="message_tutorial" class="tutorialModal">
		<div class="tutorial-modal-content">
			<p><?= getLocalised('MESSAGE_TUTORIAL'); ?></p>
			<a class="howto" href="tutorial.php"><?= getLocalised('FULL_TUTORIAL_LINK'); ?></a>&nbsp;&nbsp;
			<button class="small_button" onclick="showDateTutorial()"><?= getLocalised('GOT_IT'); ?></button>
		</div>
	</div>
		<div id="date_tutorial" class="tutorialModal">
		<div class="tutorial-modal-content">
			<p><?= getLocalised('OPENING_DATE_TUTORIAL'); ?></p>
			<a class="howto" href="tutorial.php"><?= getLocalised('FULL_TUTORIAL_LINK'); ?></a>&nbsp;&nbsp;
			<button class="small_button" onclick="showEncloseBtnTutorial()"><?= getLocalised('GOT_IT'); ?></button>
		</div>
	</div>
		<div id="enclosebtn_tutorial" class="tutorialModal">
		<div class="tutorial-modal-content">
			<p><?= getLocalised('ENCLOSE_BTN_TUTORIAL'); ?></p>
			<a class="howto" href="tutorial.php"><?= getLocalised('FULL_TUTORIAL_LINK'); ?></a>&nbsp;&nbsp;
			<button class="small_button" onclick="closeTutorial()"><?= getLocalised('GOT_IT'); ?></button>
		</div>
	</div>
	</body>
</html>