<?php 
include_once 'lang/language.php'; 
include_once 'config/google-config.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_CAPSULE'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<?php include 'logic/loginCheck.php'; ?>
		<!-- Google API -->
		<meta name="google-signin-client_id" content="<?=CLIENT_ID?>">
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<!-- End of Google API-->
		<script src ="downloaded/jquery/jquery-3.1.1.min.js"></script><!-- google log out jaoks-->
		<script src ="scripts/lateLoader.js"></script>
	</head>
	<body>

	<div id="topBar">
			<table id="top_menu">
				<tr>
					<td id="icon_box"><a href="user.php" data-logo-url="images/tcLogo.png"></a></td>
					<td id="name"><span><?php echo getName($mysqli, $_SESSION['username']);?></span></td>
					<td id="logout">
						<form id="logout_button" action="logic/logout.php"><button class="buttonInverted"><?= getLocalised('LOGOUT'); ?></button></form>
						<script src="scripts/googleAPI.js"></script><!-- google log out jaoks-->
						<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script><!-- google log out jaoks-->
					</td>
				</tr>
			</table>
	</div>
	<?php $id = (isset($_POST['id']) ? $_POST['id'] : '<NO_ID>'); ?>
	<div id="mainContainer">
		<div id="pageContent">
			<p><?php 
				if ($id == '<NO_ID>') echo getLocalised('NO_ID'); 
				else echo getLocalised('CAPSULE') . $id; 
			?></p>
			<br>
			<p><?= getLocalised('MESSAGE'); ?></p>
			<div id="messageBox">
				<div id="message">
					<?php
						if ($id != '<NO_ID>') {
							$message = getCapsuleMessage($mysqli, $id, $_SESSION['username']);  
							if ($message == '<ERROR>') echo getLocalised('TOO_SOON');
							else echo $message;
						}
					?>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>