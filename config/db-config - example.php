<?php
/**
 * These are the database login details
 */  
define("HOST", "127.0.0.1");     // The host you want to connect to.
define("USER", "root");    // The database username. 
define("PASSWORD", "");    // The database password. 
define("DATABASE", "timecapsule");    // The database name.
 
define("CAN_REGISTER", "any");
define("DEFAULT_ROLE", "member");
?>