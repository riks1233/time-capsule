<?php
include_once 'config/google-config.php';
include 'logic/loginCheck.php';

if (!isset($_POST['id'])) {
	header("Location: /user.php");
	exit;
}

if (isset($_POST['id']) && isset($_POST['confirmed']) && $_POST['confirmed']==1) {
	deleteCapsule($mysqli, $_POST['id'], $_SESSION['username']);
	header("Location: /user.php");
	exit;
}

include_once 'lang/language.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_MAIN'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<!-- Google API -->
		<meta name="google-signin-client_id" content="<?=CLIENT_ID?>">
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<!-- End of Google API-->
		<script src ="downloaded/jquery/jquery-3.1.1.min.js"></script><!-- google log out jaoks-->
		<script src="scripts/tutorial_user.js"></script>
		<script src ="scripts/lateLoader.js"></script>
	</head>
	<body>
		<div id="topBar">
			<table id="top_menu">
				<tr>
					<td id="icon_box"><a href="user.php" data-logo-url="images/tcLogo.png"></a></td>
					<td id="name"><span><?php echo getName($mysqli, $_SESSION['username']);?></span></td>
					<td id="logout">
						<form id="logout_button" action="logic/logout.php"><button class="buttonInverted"><?= getLocalised('LOGOUT'); ?></button></form>
						<script src="scripts/googleAPI.js"></script><!-- google log out jaoks-->
						<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script><!-- google log out jaoks-->
					</td>
				</tr>
			</table>
		</div>
		<div id="mainContainer">
			<div id="pageContent">
				<p><?= getLocalised('AREYOUSURE_1') . $_POST['id'] . "."; ?></p>
				<br>
				<p><?= getLocalised('AREYOUSURE_2'); ?></p>
				<br>
				<form method="POST" action="deleteCapsule.php">
					<input type="hidden" name="id" value="<?= $_POST['id'] ?>">
					<input type="hidden" name="confirmed" value="1">
					<button type="submit" class="small_button"><?= getLocalised('IMSURE'); ?></button>
				</form>
				<br>
				<form action="user.php">
					<button class="small_button"><?= getLocalised('NOTSURE'); ?></button>
				</form>
			</div>
		</div>
	</body>