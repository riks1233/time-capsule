<?php
include_once 'lang/language.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_DONATE'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>

	<div id="mainContainer">
		<div id="pageContent">
			<p><?= getLocalised('MK_DONATION'); ?></p>
			<form action="donateConfirm.php" method="post">
				<input type="number" class="inputField" placeholder="<?= getLocalised('AMOUNT_EUR'); ?>" name="amount" required>
				<br>
				<input type="submit" class="small_button" value="<?= getLocalised('DONATE'); ?>">
			</form>
			<br><br>
			<a href="index.php" class="howto"><?= getLocalised('BACK_TO_MAIN'); ?></a>
		</div>
	</div>
	</body>

</html>