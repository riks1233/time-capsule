<?php
include_once 'lang/language.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_DONATE'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
	
	<?php

//has to be changed on another machine
$private_key = openssl_pkey_get_private(
"-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA8bOm+M9zmbmBmZbvCkR1zvPW5S0+/pN5Wc0cczDCfJJUL6na
bI6FPHPOpmpk6olCH6dpmjiwcDe3JrlhlSU0EcQAmJFj/zZg7PTjjQzlZVr9wwEB
0dgAM7E8gUueSgKZUrdPK5Xhd01ilw63MCc6yOD0E9Gvq8KKgudqzAaalB/noLBa
Djgvl1eHQ2yGV/ySSHloTywc+TPue7N1O76gRAmDUAG92SagY4GomCytCqiXECUo
wdssX5j2iZ98hqRn9ZV11V//ePXo5oDg7wbCFz6pf0NOjsRohebuhime1HRYcYPg
dukyMVWd3+omDvpNJQK+ITDTnI7WnuhylB0pCQIDAQABAoIBAEGLIkBeoU6Si56G
25PBNOFGJoRHXMnUXBgKEyj5lAQL+mBigyTlESRvkx51w6AGULQcxfpYXSBRO414
wAmd6RzYNgEpjCBtMqOPVRWu5ZYyspXq8/9fv85Exc+aHfab9dAEfaeoIVgk2BN2
nh6kxMAsoIOvXSAKxVzyPHfPYqO/7DfMVd5xMoG5Imgn4TKRR3I8qtUoKjrUfGDE
YBembda+O4/hBb/kixB9es4tMaAB6fiM5aTV+BDc3fU6NrlTsDKH7CcfEksLpXtp
5x1A4JHzKqIDSW27MUST2S8dSrRLZA9yXCqtK2VigJYy52wbOQBA+p7s4nHfXS46
ni59psECgYEA/CVom3199v13Q1A5F7BnC9GFFd/obYCcHEiUk/j88qbmlCWTZLg2
cyPfbHRP5zi/NE2oPnUxl0Ew0E2dDQ5o3LI+6B/wEOn6UdppdlP/Dchp+c2XPRgW
vzr3O2sNTql6/rxYsugpEaaDouL3rxzWLX//sfHIx8i0jpWtzfaJZT0CgYEA9WVg
jdZm5AhHV+zUdQPahgK8W9uaH1LQTIGWI2gdYsANaMS5v2ODLVlS53IND2G8EaeY
p+XRjpErfmsjn+BB3C91OcWjpzIwTbZOWSjFtGHoCa1wNd0sUllwrr3ny9o4UpRo
B2Yq7CFLAkupYXfkyODZZ8zisl0TDKpy/PyXx70CgYEAvWBDTvVT1FvOZUotdzNQ
UboNYL6IzSWZGk0Q+oo5QO5Fo+MM7srEann45nX9RUaDpP7ma/KNSnL1/J0uZY37
CDb/tEu4fJufHDcUmoC/wVRS9AGLo6EiBIJ/Cat/lSMJc6V2YPipT85mc8JQEL9x
xT6rhrR3/kUqByU4IscVrmkCgYAgtBgADxi7Pxcy8o8F4TOYNHkhzMxnbEPyTjrS
ESgJZwricYVHql1rB8JysdJnwGCuAaXoiko5M8OWH3Up81sTmweGYX6D475oPOmI
HFTsyzd/Qqv3obgsaVsJDqn7kuN6reiHQ+hoe69haO/z22SUGNMgcD+uAZVuVLDq
YkdgCQKBgAkHeh4p4Vo9e62O/1wovvdZHVZXdwQ8FjpsFt+zcc5MFNkFrPAvq+l+
u+W2djgyuzOKeSRPz7e6tTcjOs7ybYZZZaXREOkQtgNDNwyJ/AF97cZZ6syKlRrW
NGsZAiyTYbImAJV+vf+ofssO9v88jniU7wncr5ZS1eGegAOWTETK
-----END RSA PRIVATE KEY-----");

if (isset($_POST["amount"])){
	$amount = $_POST["amount"];
}
else $amount = "5";
if (isset($_SERVER['HTTPS'])){
	$link_start = "https://";
}
else $link_start = "http://";

$root = $_SERVER['SERVER_NAME'];
$fields = array(
        "VK_SERVICE"     => "1011",
        "VK_VERSION"     => "008",
        "VK_SND_ID"      => "uid100049", //has to be changed on another machine
        "VK_STAMP"       => "12345",
        "VK_AMOUNT"      => $amount,
        "VK_CURR"        => "EUR",
        "VK_ACC"         => "EE152200221234567897",
        "VK_NAME"        => "Ajakapsel OÜ",
        "VK_REF"         => "1234561",
        "VK_MSG"         => "Donation",
        "VK_RETURN"      => $link_start . $root . "/donateFinal.php",//CHANGE TO HTTP FOR IT TO WORK WITH LOCALHOST
        "VK_CANCEL"      => $link_start . $root . "/donateFinal.php",//CHANGE TO HTTP FOR IT TO WORK WITH LOCALHOST
        "VK_DATETIME"    => date(DATE_ISO8601),
        "VK_ENCODING"    => "utf-8",
);

$data = str_pad (mb_strlen($fields["VK_SERVICE"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_SERVICE"] .    /* 1011 */
        str_pad (mb_strlen($fields["VK_VERSION"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_VERSION"] .    /* 008 */
        str_pad (mb_strlen($fields["VK_SND_ID"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_SND_ID"] .     /* uid100010 */
        str_pad (mb_strlen($fields["VK_STAMP"], "UTF-8"),   3, "0", STR_PAD_LEFT) . $fields["VK_STAMP"] .      /* 12345 */
        str_pad (mb_strlen($fields["VK_AMOUNT"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_AMOUNT"] .     /* 150 */
        str_pad (mb_strlen($fields["VK_CURR"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_CURR"] .       /* EUR */
        str_pad (mb_strlen($fields["VK_ACC"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_ACC"] .        /* EE152200221234567897 */
        str_pad (mb_strlen($fields["VK_NAME"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_NAME"] .       /* Ajakapsel OÜ */
        str_pad (mb_strlen($fields["VK_REF"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_REF"] .        /* 1234561 */
        str_pad (mb_strlen($fields["VK_MSG"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_MSG"] .        /* Torso Tiger */
        str_pad (mb_strlen($fields["VK_RETURN"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_RETURN"] .     /* http://localhost:8080/project/TwbesUbDv7lqZI8w?payment_action=success */
        str_pad (mb_strlen($fields["VK_CANCEL"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_CANCEL"] .     /* http://localhost:8080/project/TwbesUbDv7lqZI8w?payment_action=cancel */
        str_pad (mb_strlen($fields["VK_DATETIME"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_DATETIME"];    /* 2017-04-07T17:49:53+0300 */

openssl_sign ($data, $signature, $private_key, OPENSSL_ALGO_SHA1);
$fields["VK_MAC"] = base64_encode($signature);
?>

	<div id="mainContainer">
		<div id="pageContent">
			<p><?= getLocalised('MK_DONATION'); ?></p>
			
			<form action="http://localhost:8080/banklink/swedbank-common" method="post"> <!--change action link for appropriate bank-->
				<?php foreach($fields as $key => $val):?>
					<input type="hidden" name="<?php echo $key; ?>" value="<?php echo htmlspecialchars($val); ?>" />
				<?php endforeach;
				echo "<p>" . getLocalised('DONATE') . " $fields[VK_AMOUNT] € ?";?>
				
				<br><br>
				<input type="submit" class="small_button" value="<?= getLocalised('CONFIRM'); ?>">
			</form>
			<br><br>
			<a href="index.php" class="howto"><?= getLocalised('BACK_TO_MAIN'); ?></a>
		</div>
	</div>
	</body>

</html>