<?php
include_once 'lang/language.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_DONATE'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
	
	<?php
//has to be changed on another machine
$public_key = openssl_pkey_get_public("-----BEGIN CERTIFICATE-----
MIIDljCCAn4CCQCtffgJO4FxejANBgkqhkiG9w0BAQUFADCBjDELMAkGA1UEBhMC
RUUxETAPBgNVBAgTCEhhcmp1bWFhMRAwDgYDVQQHEwdUYWxsaW5uMQ0wCwYDVQQK
EwRUZXN0MREwDwYDVQQLEwhiYW5rbGluazEXMBUGA1UEAxMObG9jYWxob3N0IDgw
ODAxHTAbBgkqhkiG9w0BCQEWDnRlc3RAbG9jYWxob3N0MB4XDTE3MDQwODA5NTIx
M1oXDTM3MDQwMzA5NTIxM1owgYwxCzAJBgNVBAYTAkVFMREwDwYDVQQIEwhIYXJq
dW1hYTEQMA4GA1UEBxMHVGFsbGlubjENMAsGA1UEChMEVGVzdDERMA8GA1UECxMI
YmFua2xpbmsxFzAVBgNVBAMTDmxvY2FsaG9zdCA4MDgwMR0wGwYJKoZIhvcNAQkB
Fg50ZXN0QGxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
AL8OpvWzi7BsqnH5B0qd71Kgf4SpzbYyEdzO2NRqfHDcZZVoH8LwYd/N4qZDoHHb
w3vb+fnzlywBZmdRG12vuOom8Wzu9KLgFaZswbNUbooSYX5S1lQXBXAPyUxec7lA
sDwJYCVsfuGRFhUNWGQG97xGvGhW5wuoTT1gNu+IWP8ISfjDXEYNV5ty+2sJ7cEa
DSFxDn3PxYfDtLIa0/oERNd1EQs+Yy6FRLHB1LVbGzCPgqd+b5apHrECuS/Mj9TJ
MPnVx13WE8SpQUyMZi1IMH/iTz+jGG1L4uTpO5zfyQJehUracaEE9qqkaQFg7z4x
5Ftiokvz39wkolnUuKfIp0kCAwEAATANBgkqhkiG9w0BAQUFAAOCAQEAj35hBYS+
vM11hbiKg0Ej2tmEPTPpRByk9IYr7XUB4hhxUchPZ8EPpLsBpdC1BpsTsUVw8XED
AL1QhE6G5H/8vFMetx/9ZEs9tdE3qHIArDFp0PHCps+0bW3qjS+CXt5CrxchxcHk
l4ryrZ7T0hP+vHznHznSyOQAz4Ii2b8H7Dz04xleLKA2qHaInM9qYRBJVTgkpY0i
tOkk8jC4SOd9h7/ICFJbLRNJuSko1P9TaIvtviZ6efitSdwHnsJNAdaCXpLeeXDB
hXw8LmTlddzedfC3k1mi8l45/430n/SbcnQNPKpIBxRw69q4Qx6DpT65QTG6PXpr
IgR5GngJxMeHDg==
-----END CERTIFICATE-----");

$fields = array();
$data = "";
foreach($_POST as $key => $value){
	$fields["$key"] = "$value";
	$data = $data . str_pad (mb_strlen($key, "UTF-8"),   3, "0", STR_PAD_LEFT) . $key;
}

if (openssl_verify ($data, base64_decode($fields["VK_MAC"]), $public_key) !== 1) {
    $signatureVerified = false;
}else{
    $signatureVerified = true;
}
?>

	<div id="mainContainer">
		<div id="pageContent">
			<br>
			<p><?php echo $fields["VK_SERVICE"] == "1111" ? getLocalised('DONATE_OK') : getLocalised('DONATE_NOT_OK') ?></p>
			<br><br>
			<a href="index.php" class="howto"><?= getLocalised('BACK_TO_MAIN'); ?></a>
		</div>
	</div>
	</body>

</html>