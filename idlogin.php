<?php 
session_start();
include_once 'logic/db_connect.php';
include_once 'logic/functions.php';

if(login_check($mysqli) == true) {
	header('Location: ./user.php');
	exit;
}
session_start();

if (isset($_SESSION['REDIRECT'])) {
	$redirect = $_SESSION['REDIRECT'];
}

session_destroy();
sec_session_start();

header ("Content-Type: text/html; charset=utf-8");
  
// Get user information obtained by Apache from the Estonian ID card.
// Return list [last_name,first_name,person_code] or False if fails. 

function get_user () {
	// Get values and save them to local variables.
	$identity=getenv("SSL_CLIENT_S_DN");  
	$verify=getenv("SSL_CLIENT_VERIFY");
  
	// Unset global variables.
	putenv("SSL_CLIENT_S_DN");
	putenv("SSL_CLIENT_VERIFY");

	// Check and parse the values
	if (!$identity || $verify!="SUCCESS") return False;
	$identity=certstr2utf8($identity); // Old cards use UCS-2, new cards use UTF-8.
	if (strpos($identity,"/C=EE/O=ESTEID")!=0) return False;
	
	$ps=strpos($identity,"/SN=");
	$pg=strpos($identity,"/GN=");
	$pc=strpos($identity,"/serialNumber=");
	if (!$ps || !$pg || !$pc) return False; // Everything else should be set too. :)
	
	$result=array(substr($identity, $ps+4,	$pg-($ps+4)), 
				  substr($identity, $pg+4,	$pc-($pg+4)), 
				  substr($identity, $pc+14, strlen($identity)-$pc+14));
	return $result;
}  

// Convert names from UCS-2/UTF-16 to UTF-8.
// Function taken from help.zone.eu.

function certstr2utf8 ($str) {
  $str = preg_replace ("/\\\\x([0-9ABCDEF]{1,2})/e", "chr(hexdec('\\1'))", $str);       
  $result="";
  $encoding=mb_detect_encoding($str,"ASCII, UCS2, UTF8");
  if ($encoding=="ASCII") {
    $result=mb_convert_encoding($str, "UTF-8", "ASCII");
  } else {
    if (substr_count($str,chr(0))>0) {
      $result=mb_convert_encoding($str, "UTF-8", "UCS2");
    } else {
      $result=$str;
    }
  }
  return $result;
}

$IDuser = get_user();
if ($IDuser == False) {
	header("Location: /error.php?err=401");
	exit;
}
$user_email = '-'; // Ei tea emaili, tulevikus vb k�sime, hetkel ei tee.
$user_name = validateInput($IDuser[1]);//turvalisus ..

$username = 'idkaart' . $IDuser[2];
	
$random_hash = bin2hex(generate_random_string(32));#uue kasutaja jaoks genereeritakse hash, mis p�rast l�heb login stringi hashi
	
$stmt1 = $mysqli->prepare("CALL register(?, ?, ?, ?, @output, 2)");#teeb asju ainult esimesel loginil
$stmt1->bind_param('ssss', $username, $user_email, $random_hash, $user_name);

$stmt1->execute();
$stmt1->close();
	
$password_hash = get_db_password_hash($mysqli, $username);//saadakse esimesel loginil genereeritud hashi
	
$user_browser = $_SERVER['HTTP_USER_AGENT'];
$_SESSION['username'] = $username;
$_SESSION['login_string'] = hash('sha512', $password_hash . $user_browser);
process_login_result(1, $redirect);
?>