<?php 
session_start();
include_once 'logic/db_connect.php';
include_once 'logic/functions.php';
include_once 'config/google-config.php';

if(login_check($mysqli) == true) {
	header('Location: ./user.php');
	exit;
}
include_once 'lang/language.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_MAIN'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<script src="downloaded/jquery/jquery-3.1.1.min.js"></script>
		<!-- Google API -->
		<meta name="google-signin-client_id" content="<?=CLIENT_ID?>">
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<!-- End of Google API-->
		<!-- testime kas jquery ja indexJS on laetud, kui ei, siis laadime teisest serverist, ja kui ei õnnestu, siis kodu serverist -->
		<script>
			var prompt1 = "<?= getLocalised('PROMPT_1')?>";
			var prompt2 = "<?= getLocalised('PROMPT_2')?>";
			var prompt3 = "<?= getLocalised('PROMPT_3')?>";
			var prompt4 = "<?= getLocalised('PROMPT_4')?>";
			var loginButtonText = "<?= getLocalised('LOGIN_BTN')?>";
			var registerButtonText = "<?= getLocalised('REGISTER_BTN')?>";
		</script>						
		<script async src="scripts/googleAPI.js"></script>
	</head>
	<body id="index">
	<?php 
		include_once './logic/rsa.php';
	?>
	

	<div id="centeredCapsule">
		<div id="centeredCapsuleContent">
			<p><?= getLocalised('WELCOME'); ?></p>
			<div id="normalButtons"></div>
			<noscript>
				<span class="noscript">
					<?= getLocalised('PROMPT_1'); ?>
					<a href="login.php"><?= getLocalised('PROMPT_2'); ?></a>
					<?= getLocalised('PROMPT_3'); ?>
					<a href="register.php"><?= getLocalised('PROMPT_4'); ?></a>
				</span><br>
				<span class="red_text"><?= getLocalised('NOSCRIPT'); ?></span>
				<p>
					<a href="login.php" class="small_button"><?= getLocalised('LOGIN_BTN'); ?></a>
				</p>
				<p>
					<a href="register.php" class="small_button"><?= getLocalised('REGISTER_BTN'); ?></a>
				</p>
			</noscript>
			<a href="./?lang=et" ><img src="images/Estonia.png" class="lang" alt="eesti"/></a>
			<a href="./?lang=en" ><img src="images/United_Kingdom.png" class="lang" alt="english"/></a>
			<a href="./?lang=ru" ><img src="images/Russia.png" class="lang" alt="русский"/></a>
			<p><a class="howto" href="about.php"><?= getLocalised('ABOUT'); ?></a></p>
		</div>
	</div>
	
	<div id="loginModal" class="modal">
	  <!-- Log in modal content -->
		<div class="modal-content">
			<span class="close" onclick="loginSpanClose()">&times;</span>
			<p><?= getLocalised('LOGIN'); ?></p>
			<form id = "login_form" action="logic/process_login.php?form=modal" method="post">
				<input title="<?= getLocalised('USERNAME'); ?>" name="username" class="inputField" type="text" maxlength="25"  placeholder="<?= getLocalised('USERNAME'); ?>" required>
				<div class="errorField"><span id="login-error"></span></div>
				<input title="<?= getLocalised('PASSWORD'); ?>" name="password" class="inputField" type="password" maxlength="30"  placeholder="<?= getLocalised('PASSWORD'); ?>" required>
				<div class="errorField">
					<?php 
						if (isset($_GET['error'])){
							echo '<script type="text/javascript">var onIndexLoad = 0</script>';
							$err = $_GET['error'];
							if($err == 0)
								echo getLocalised('LOGIN_ERR_NULL');
							elseif ($err == 2)
								echo getLocalised('LOGIN_ERR_TWO');
							elseif ($err == 3)
								echo getLocalised('LOGIN_ERR_THREE');
						} 
					?>
				</div>
				<p>
					<input type="submit" value="<?= getLocalised('LOGIN_BTN'); ?>" class="small_button">
				</p>
				<a href="https://id.ajakapsel.me/idlogin.php"><img src="images/IDcard.gif" alt="<?= getLocalised('ID_LOGIN'); ?>"/></a>
				<?= getLocalised('OR') ?>
				<div class="g-signin2" data-onsuccess="onSignIn"></div>
			</form>
		</div>
	</div>
	
	
	<div id="registerModal" class="modal">
	  <!-- Register modal content -->
		<div class="modal-content">
			<span class="close" onclick="registerSpanClose()">&times;</span>
			<p><?= getLocalised('REGISTER'); ?></p>
			<form id = "register_form" action="logic/validate.php?form=modal" method="post">
				<input title="<?= getLocalised('USERNAME'); ?>" name="reg_username" class="inputField" type="text" placeholder="<?= getLocalised('USERNAME'); ?>" maxlength="25" minlength="5" required value="<?php if(isset($_GET["reg_username"]))echo $_GET["reg_username"]?>">
				<div class="errorField">
					<?php 
						if (isset($_GET["err_username"])) {
						echo '<script type="text/javascript">var onIndexLoad = 1</script>';
						$err_username = $_GET["err_username"];
						if ($err_username == 0)
							echo getLocalised('REGISTER_ERR_USER_NULL');
						elseif ($err_username == 1)
							echo getLocalised('REGISTER_ERR_USER_ONE');
						elseif ($err_username == 2)
							echo getLocalised('REGISTER_ERR_USER_TWO');
					} ?>
				</div>
				<input title="<?= getLocalised('PASSWORD'); ?>" name="reg_password" class="inputField" type="password" placeholder="<?= getLocalised('PASSWORD'); ?>" maxlength="30" minlength="8" required>
				<div class="errorField"></div>
				<input title="<?= getLocalised('REGISTER_PASS_REPEAT'); ?>" name="reg_pass_repeat" class="inputField" type="password" placeholder="<?= getLocalised('REGISTER_PASS_REPEAT'); ?>" maxlength="30" minlength="8" required>
				<div class="errorField">
					<?php 
						if (isset($_GET["err_password"])) {
							echo '<script type="text/javascript">var onIndexLoad = 1</script>';
							$err_password = $_GET["err_password"];
							if ($err_password == 0)
								echo getLocalised('REGISTER_ERR_PASS');
						} 
					?>
				</div>
				<input title="<?= getLocalised('REGISTER_NAME'); ?>" name="reg_name" class="inputField" type="text" placeholder="<?= getLocalised('REGISTER_NAME'); ?>" maxlength="50"  value="<?php if(isset($_GET["reg_name"]))echo $_GET["reg_name"]?>">
				<div class="errorField"></div>
				<input title="<?= getLocalised('REGISTER_EMAIL'); ?>" name="reg_email" class="inputField" type="email" placeholder="<?= getLocalised('REGISTER_EMAIL'); ?>" maxlength="50" required value="<?php if(isset($_GET["reg_email"]))echo $_GET["reg_email"]?>">
				<div class="errorField"></div>
				<input title="<?= getLocalised('REGISTER_EMAIL_REPEAT'); ?>" name="reg_email_repeat" class="inputField" type="email" placeholder="<?= getLocalised('REGISTER_EMAIL_REPEAT'); ?>" maxlength="50" required value="<?php if(isset($_GET["reg_email_repeat"]))echo $_GET["reg_email_repeat"]?>">
				<div class="errorField">
					<?php 
						if (isset($_GET["err_email"])) {
							echo '<script type="text/javascript">var onIndexLoad = 1</script>';
							$err_email = $_GET["err_email"];
							if ($err_email == 0)
								echo getLocalised('REGISTER_ERR_NULL');
							elseif ($err_email == 1)
								echo getLocalised('REGISTER_ERR_ONE');
							elseif ($err_email == 2)
								echo getLocalised('REGISTER_ERR_TWO');
						} 
					?>
				</div>
				<p>
					<input type="submit" value="<?= getLocalised('REGISTER_BTN'); ?>" class="small_button">
				</p>
			</form>
			<script src="scripts/rsa/rsaAsync.js"></script>
			<script src="scripts/cdn/index.js"></script>	
		</div>
	</div>
	</body>

</html>