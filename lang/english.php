<?php

$english = array();

// OVERALL
$english['LANG'] = 'en';
$english['LOGOUT'] = 'Log out';
$english['PLACEHOLDER'] = 'FUS RO DAH!';
$english['TITLE'] = 'Time Capsule'; //old

// TITLES
$english['TITLE_MAIN'] = 'Time Capsule';
$english['TITLE_ABOUT'] = 'About';
$english['TITLE_CREATION'] = 'Capsule creation';
$english['TITLE_CAPSULE'] = 'Capsule';
$english['TITLE_DONATE'] = 'Donation';
$english['TITLE_LOGIN'] = 'Login';
$english['TITLE_REGISTER'] = 'Registration';
$english['TITLE_TUTORIAL'] = 'Tutorial';

// INDEX OVERALL
$english['LOGIN'] = 'Log in';
$english['REGISTER'] = 'Register';
$english['USERNAME'] = 'Username';
$english['PASSWORD'] = 'Password';

$english['WELCOME'] = 'Welcome to the Time Capsule!<br><br><br>
					The easiest way to send and track multiple messages<br>
					into the future.';

$english['PROMPT_1'] = 'Please'; 
$english['PROMPT_2'] = 'log in'; 
$english['PROMPT_3'] = 'or'; 
$english['PROMPT_4'] = 'register'; 
$english['NOSCRIPT'] = 'JavaScript is not supported!'; 
$english['OR'] = 'or';
$english['ABOUT'] = 'About us';

// Login page/modal.
$english['LOGIN_ERR_NULL'] = 'Invalid username or password.';
$english['LOGIN_ERR_TWO'] = 'Too many login attempts.';
$english['LOGIN_ERR_THREE'] = 'Please log in or register.';
$english['LOGIN_BTN'] = 'Log in';

// ID login.
$english['ID_LOGIN_PROMPT'] = 'Press "Log in" to continue login with ID-card.';
$english['ID_LOGIN'] = 'ID-card';

// Register page/modal.
$english['REGISTER_ERR_USER_NULL'] = 'Use only letters, numbers and underscores.';
$english['REGISTER_ERR_USER_ONE'] = 'Username already in use.';
$english['REGISTER_ERR_USER_TWO'] = '"google+numbers" not allowed.';
$english['REGISTER_PASS_REPEAT'] = 'Repeat Password';
$english['REGISTER_ERR_PASS'] = 'Passwords do not match.';
$english['REGISTER_NAME'] = 'Name (Optional)';
$english['REGISTER_EMAIL'] = 'E-mail';
$english['REGISTER_EMAIL_REPEAT'] = 'Repeat E-mail';
$english['REGISTER_ERR_NULL'] = 'Emails do not match.';
$english['REGISTER_ERR_ONE'] = 'Invalid email format.';
$english['REGISTER_ERR_TWO'] = 'Email already in use.';
$english['REGISTER_BTN'] = 'Register';

// USER OVERALL
$english['CREATE'] = 'New Capsule';
$english['MESSAGE'] = 'Message';
$english['TUTORIAL'] = 'Help';
$english['CAPSULE'] = 'Capsule #';

$english['NO_ID'] = 'No ID specified for the capsule. Please go back to the user page and try again. :O';
$english['TOO_SOON'] = 'Capsule has not yet opened, or wrong ID/user (unlikely).';

$english['OPEN'] = 'Open';

// User page
$english['USER_CAPSULES'] = 'My capsules:';

// Creation page
$english['CREATE_MESSAGE_TITLE'] = 'Message to send';
$english['CREATE_OPEN'] = 'Opening date';
$english['CREATE_OPEN_TITLE'] = 'Opening date and time';
$english['CREATE_ERR_ONE'] = 'Invalid date or time.';
$english['CREATE_ERR_NULL'] = 'Invalid format, use YYYY-MM-DD HH:mm format. (no JS)';
$english['CREATE_ENCLOSE'] = 'Enclose';

// Delete page
$english['AREYOUSURE_1'] = "You are about to delete capsule #";
$english['AREYOUSURE_2'] = "Are you sure you wish to continue? <br> You will NOT see the message inside if the capsule is still closed.";
$english['IMSURE'] = "I'm sure";
$english['NOTSURE'] = "NO!";

// Tutorial page
$english['TUTORIAL_HEADLINE'] = 'To create a new capsule press the "New Capsule" button.';
$english['TUTORIAL_TEXT'] = 'In the capsule creation page enter the message to be enclosed and the opening date of the capsule. Press "Enclose".
							All capsules will be listed at your user page in the "My capsules" list.
							You can click on one of them to go to the capsule page to see the time left.
							However, it is impossible to open a capsule before the opening date.<br>
							When a capsule is ready, You recieve a notification to your e-mail.<br>
							After that, You will be able to open the capsule at that specific capsule page (a button will appear).';

$english['FULL_TUTORIAL_LINK'] = "Full tutorial";
$english['GOT_IT'] = "Got it!";

// Tutorial USER page
$english['NEW_CAPSULE_BTN_TUTORIAL'] = "Click on the button below to create a new capsule";
$english['MY_CAPSULES_TUTORIAL'] = "All capsules will be listed in the table below until manually deleted";

// Tutorial CAPSULE CREATION page
$english['MESSAGE_TUTORIAL'] = "Enter the message You would like to enclose in the box below";
$english['OPENING_DATE_TUTORIAL'] = "Choose the opening date for your message below";
$english['ENCLOSE_BTN_TUTORIAL'] = "Click on the button below to enclose the message";

// About page
$english['FIND'] = 'Where to find us?';
$english['MK_DONATION'] = 'Make a donation';
$english['CONTACT_INFO'] = 'Contact Info';
$english['REPO'] = 'Repository';

// Bank
$english['AMOUNT_EUR'] = 'Amount (EUR)';
$english['CONFIRM'] = 'Confirm';
$english['DONATE'] = 'Donate';
$english['DONATE_OK'] = 'Thank you for Your donation!';
$english['DONATE_NOT_OK'] = 'Something went wrong :( Payment was not performed';
$english['BACK_TO_MAIN'] = 'Back to main';
?>