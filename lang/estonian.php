<?php

$lang = array();

// OVERALL
$lang['LANG'] = 'et';
$lang['LOGOUT'] = 'Logi välja';
$lang['PLACEHOLDER'] = 'FUS RO DAH!';
$lang['TITLE'] = 'Ajakapsel';

// TITLES
$lang['TITLE_MAIN'] = 'Ajakapsel';
$lang['TITLE_ABOUT'] = 'Meist';
$lang['TITLE_CREATION'] = 'Kapsli loomine';
$lang['TITLE_CAPSULE'] = 'Kapsel';
$lang['TITLE_DONATE'] = 'Annetamine';
$lang['TITLE_LOGIN'] = 'Sisselogimine';
$lang['TITLE_REGISTER'] = 'Registreerimine';
$lang['TITLE_TUTORIAL'] = 'Õpetus';

// INDEX OVERALL
$lang['LOGIN'] = 'Sisselogimine';
$lang['REGISTER'] = 'Registreerimine';
$lang['USERNAME'] = 'Kasutajatunnus';
$lang['PASSWORD'] = 'Parool';

$lang['WELCOME'] = 'Tere tulemast Time Capsule kodulehele!<br><br><br>
					Lihtsaim viis saata ning jälgida mitmeid sõnumeid<br>
					tulevikku.';

$lang['PROMPT_1'] = 'Palun'; 
$lang['PROMPT_2'] = 'logi sisse'; 
$lang['PROMPT_3'] = 'või'; 
$lang['PROMPT_4'] = 'registreeru'; 
$lang['NOSCRIPT'] = 'JavaScript ei ole toetatud!'; 
$lang['OR'] = 'või';
$lang['ABOUT'] = 'Meist';

// Login page/modal.
$lang['LOGIN_ERR_NULL'] = 'Vale kasutajatunnus või parool.';
$lang['LOGIN_ERR_TWO'] = 'Liiga palju sisselogimiskatseid.';
$lang['LOGIN_ERR_THREE'] = 'Palun logi sisse või registreeru.';
$lang['LOGIN_BTN'] = 'Logi sisse';

// ID login.
$lang['ID_LOGIN_PROMPT'] = 'Vajutage "Logi sisse" et jätkata logimist ID-kaardiga.';
$lang['ID_LOGIN'] = 'ID-kaart';

// Register page/modal.
$lang['REGISTER_ERR_USER_NULL'] = 'Ainult ladina tähed, numbrid ja alakriipsud.';
$lang['REGISTER_ERR_USER_ONE'] = 'Kasutajatunnus juba kasutusel.';
$lang['REGISTER_ERR_USER_TWO'] = '"google+numbrid" ei ole lubatud. :(';
$lang['REGISTER_PASS_REPEAT'] = 'Korda parooli';
$lang['REGISTER_ERR_PASS'] = 'Paroolid ei kattu.';
$lang['REGISTER_NAME'] = 'Nimi (Valikuline)';
$lang['REGISTER_EMAIL'] = 'Email';
$lang['REGISTER_EMAIL_REPEAT'] = 'Korda emaili';
$lang['REGISTER_ERR_NULL'] = 'Emailid ei kattu.';
$lang['REGISTER_ERR_ONE'] = 'Vale emaili formaat.';
$lang['REGISTER_ERR_TWO'] = 'Email on juba kasutuses.';
$lang['REGISTER_BTN'] = 'Registreeru';

// USER OVERALL
$lang['CREATE'] = 'Uus Kapsel';
$lang['MESSAGE'] = 'Sõnum';
$lang['TUTORIAL'] = 'Abi';
$lang['CAPSULE'] = 'Kapsel #';

$lang['NO_ID'] = ' Ajakapsli ID ei ole määratud. Palun minge tagasi kasutaja pealehele ning proovige uuesti. :O';
$lang['TOO_SOON'] = 'Ajakapsel pole veel avanenud, või on vale ID/kasutaja (ebatõenäoline).';

$lang['OPEN'] = 'Ava';

// User page
$lang['USER_CAPSULES'] = 'Minu kapslid:';

// Creation page
$lang['CREATE_MESSAGE_TITLE'] = 'Sõnum saatmiseks';
$lang['CREATE_OPEN'] = 'Avanemise aeg';
$lang['CREATE_OPEN_TITLE'] = 'Avanemise kuupäev ja kellaaeg';
$lang['CREATE_ERR_ONE'] = 'Vale kuupäev või kellaaeg.';
$lang['CREATE_ERR_NULL'] = 'Vale formaat, kasuta formaati YYYY-MM-DD HH:mm. (JS ei tööta)';
$lang['CREATE_ENCLOSE'] = 'Sulge';

// Delete page
$lang['AREYOUSURE_1'] = "Olete kustutamas kapslit #";
$lang['AREYOUSURE_2'] = "Kas olete kindel, et soovite jätkata? <br> Te EI näe kapslis olevat sõnumit, kui kapsel ei ole veel avanenud.";
$lang['IMSURE'] = "Kindel";
$lang['NOTSURE'] = "EI!";

// Tutorial page
$lang['TUTORIAL_HEADLINE'] = 'Uue kapsli loomiseks vajutage nupule "Uus Kapsel".';
$lang['TUTORIAL_TEXT'] = 'Kapslite loomise lehel sisestage sõnum kapslisse ning määrake avanemise aeg. Seejärel vajutage nupule "Sulge".
							Kõiki enda poolt loodud aktiivseid kapsleid näete oma kasutajalehelt, "Minu kapslid" loetelus.
							Kapsli avanemiseni jääva aja vaatamiseks vajutage selle kindla kapsli peale, mille aega näha soovite.
							Pidage meeles, et enne avanemise aega ei ole Teil võimalik kapsli sisu uuesti näha.<br>
							Kui kapsel on valmis avamiseks, teavitatakse Teid sõnumiga emailile.<br>
							Peale aja möödumist on Teil võimalik kapsel avada lehel, kus muidu näitab kapsli avanemiseni järelejäänud aega (tekib avamiseks mõeldud nupp).';

$lang['FULL_TUTORIAL_LINK'] = "Pikem õpetus";
$lang['GOT_IT'] = "Sain aru!";

// Tutorial USER page
$lang['NEW_CAPSULE_BTN_TUTORIAL'] = "Uue kapsli loomiseks vajuta alumisele nupule \"Uus Kapsel\"";
$lang['MY_CAPSULES_TUTORIAL'] = "Kõik loodud kapslid on loetletud allpool olevas tablelis, kuni te need kustutate";

// Tutorial CAPSULE CREATION page
$lang['MESSAGE_TUTORIAL'] = "Sisestage sõnum, mida soovite kapslisse sisestada, allpool olevasse kasti";
$lang['OPENING_DATE_TUTORIAL'] = "Valige oma kapsli avanemise aeg allpool olevas menüüs";
$lang['ENCLOSE_BTN_TUTORIAL'] = "Vajutage allpool olevale nupule, et sulgeda oma sõnum kapslisse";
					
// About page
$lang['FIND'] = 'Kust meid leida?';
$lang['MK_DONATION'] = 'Tee annetus';
$lang['CONTACT_INFO'] = 'Kontaktandmed';
$lang['REPO'] = 'Repositoorium';

// Bank
$lang['AMOUNT_EUR'] = 'Summa (EUR)';
$lang['CONFIRM'] = 'Kinnita';
$lang['DONATE'] = 'Anneta';
$lang['DONATE_OK'] = 'Aitäh annetuse eest!';
$lang['DONATE_NOT_OK'] = 'Midagi läks valesti :( Makset ei sooritatud.';
$lang['BACK_TO_MAIN'] = 'Avalehele';
?>