<?php
if(isset($_GET['lang'])) {
	setcookie('lang', $_GET['lang'], time() + (60 * 60 * 24 * 30)); // sec * min * hours * days
	header("Location: ./"); // "The setcookie() function defines a cookie to be sent along with the rest of the HTTP headers." - via https://www.w3schools.com/
	exit; 
}

if(isset($_COOKIE['lang'])){
    if ($_COOKIE['lang'] == 'et')
	    include 'estonian.php';	
	elseif ($_COOKIE['lang'] == 'ru')
        include 'russian.php';
}

include 'english.php'; // Default.

function getLocalised($id) {
	global $lang, $english;
	
	if (isset($lang[$id]))
		return $lang[$id];
	elseif (isset($english[$id]))
		return $english[$id];
	return 'PLACEHOLDER'; // Something is missing.
}
?>