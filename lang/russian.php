<?php

$lang = array();

// OVERALL
$lang['LANG'] = 'ru';
$lang['LOGOUT'] = 'Выход'; //'Log out';
$lang['PLACEHOLDER'] = 'FUS RO DAH!';
$lang['TITLE'] = 'Капсула Времени';

// TITLES
$lang['TITLE_MAIN'] = 'Капсула Времени'; //'Time Capsule';
$lang['TITLE_ABOUT'] = 'О нас';
$lang['TITLE_CREATION'] = 'Создание капсулы';
$lang['TITLE_CAPSULE'] = 'Капсула';
$lang['TITLE_DONATE'] = 'Пожертвование';
$lang['TITLE_LOGIN'] = 'Вход';
$lang['TITLE_REGISTER'] = 'Регистрация';
$lang['TITLE_TUTORIAL'] = 'Инструкция';

// INDEX OVERALL
$lang['LOGIN'] = 'Вход'; //'Log in';
$lang['REGISTER'] = 'Регистрация'; //'Register';
$lang['USERNAME'] = 'Имя пользователя'; //'Username';
$lang['PASSWORD'] = 'Пароль'; //'Password';

$lang['WELCOME'] = 'Добро пожаловать в Капсулу Времени!<br><br><br>
					Самый лёгкий способ отправки и отслеживания сообщений<br>
					в будущее.';
					/*'Welcome to the Time Capsule!<br><br><br>
					The easiest way to send and track multiple messages<br>
					into the future.';*/

$lang['PROMPT_1'] = 'Пожалуйста'; //'Please '; 
$lang['PROMPT_2'] = 'войдите'; //'log in '; 
$lang['PROMPT_3'] = 'или'; //'or '; 
$lang['PROMPT_4'] = 'зарегистрируйтесь'; //'register'; 
$lang['NOSCRIPT'] = 'JavaScript не поддерживается!'; //'JavaScript is not supported!';
$lang['OR'] = 'или'; 
$lang['ABOUT'] = 'О нас'; //'About us';

// Login page/modal.
$lang['LOGIN_ERR_NULL'] = 'Неверное имя пользователя или пароль.'; //'Invalid username or password.';
$lang['LOGIN_ERR_TWO'] = 'Слишком много попыток входа.'; //'Too many login attempts!';
$lang['LOGIN_ERR_THREE'] = 'Пожалуйста войдите или зарегистрируйтесь.'; //'Please log in or register.';
$lang['LOGIN_BTN'] = 'Войти'; //'Log in';

// ID login.
$lang['ID_LOGIN_PROMPT'] = 'Нажите "Вход", чтобы продолжить вход с ID-картой';//'Press "Log in" to continue login with ID-card.';
$lang['ID_LOGIN'] = 'ИД карта';

// Register page/modal.
$lang['REGISTER_ERR_USER_NULL'] = 'Используйте только латиницу, цифры и подчеркивания.'; //'Use only letters, numbers and underscores!';
$lang['REGISTER_ERR_USER_ONE'] = 'Имя пользователя занято.'; //'Username already in use.';
$lang['REGISTER_ERR_USER_TWO'] = 'FUS RO DAH'; //'"google+numbers" not allowed. :(';
$lang['REGISTER_PASS_REPEAT'] = 'Повторите пароль'; //'Repeat Password';
$lang['REGISTER_ERR_PASS'] = 'Пароль не сходится.'; //'Passwords do not match!';
$lang['REGISTER_NAME'] = 'Имя (необязательно)'; //'Name (Optional)';
$lang['REGISTER_EMAIL'] = 'Е-майл'; //'E-mail';
$lang['REGISTER_EMAIL_REPEAT'] = 'Повторите Е-майл'; //'Repeat E-mail';
$lang['REGISTER_ERR_NULL'] = 'Е-майл не сходится.'; //'Emails do not match.';
$lang['REGISTER_ERR_ONE'] = 'Неверный формат Е-майла.'; //'Invalid email format!';
$lang['REGISTER_ERR_TWO'] = 'Е-майл занят.'; //'Email already in use!';
$lang['REGISTER_BTN'] = 'Регистрация'; //'Register';

// USER OVERALL
$lang['CREATE'] = 'Новая Капсула'; //'New Capsule';
$lang['MESSAGE'] = 'Сообщение'; //'Message';
$lang['TUTORIAL'] = 'Помощь'; //'How To';
$lang['CAPSULE'] = 'Капсула #'; //'Capsule #';

$lang['NO_ID'] = 'FUS RO DAH!'; //'No ID specified for the capsule. Please go back to the user page and try again. :O';
$lang['TOO_SOON'] = 'FUS RO DAH!'; //'Capsule has not yet opened, or wrong ID/user (unlikely).';

$lang['OPEN'] = 'Открыть'; //'Open';

// User page
$lang['USER_CAPSULES'] = 'Мои капсулы'; //'My capsules:';

// Creation page
$lang['CREATE_MESSAGE_TITLE'] = 'Сообщение для отправки'; //'Message to send';
$lang['CREATE_OPEN'] = 'Дата открытия'; //'Opening date';
$lang['CREATE_OPEN_TITLE'] = 'Дата открытия и время'; //'Opening date and time';
$lang['CREATE_ERR_ONE'] = 'Неверная дата или время.'; //'Invalid date or time.';
$lang['CREATE_ERR_NULL'] = 'Неверный формат, используйте формат ГГГГ-ММ-ДД ЧЧ:мм. (JS не работает)'; //'Invalid format, use YYYY-MM-DD HH:mm format. (no JS)';
$lang['CREATE_ENCLOSE'] = 'Закрыть'; //'Enclose';

// Delete page
$lang['AREYOUSURE_1'] = 'FUS RO DAH!'; //"You are about to delete capsule #";
$lang['AREYOUSURE_2'] = 'FUS RO DAH!'; //"Are you sure you wish to continue? <br> You will NOT see the message inside if the capsule is still closed.";
$lang['IMSURE'] = 'FUS RO DAH!'; //"I'm sure";
$lang['NOTSURE'] = 'FUS RO DAH!'; //"NO!";

// Tutorial page
$lang['TUTORIAL_HEADLINE'] = 'Чтобы создать новую капсулу нажмите кнопку "Новая Капсула".'; //'To create a new capsule press the "New Capsule" button.';
$lang['TUTORIAL_TEXT'] = 'На странице создания капсулы введите сообщение и дату открытия капсулы. Нажмите "Закрыть".
						Список всех капсул "Мои капсулы" находится на Вашей странице пользователя.
						Вы можете нажать на одну из них, чтобы перейти на страницу выбранной капсулы.
						Однако, открытие капсулы до назначенного времени невозможно.
						По истечению назначенного времени капсулы Вам на почту приходит оповещение.
						После, Вы сможете открыть капсулу на странице данной капсулы (на ней появится кнопка).';
						/*'In the capsule creation page enter the message to be enclosed and the opening date of the capsule. Press "Enclose".
						All capsules will be listed at your user page in the "My capsules" list.
						You can click on one of them to go to the capsule page to see the time left.
						However, it is impossible to open a capsule before the opening date.<br>
						When a capsule is ready, you recieve a notification to your e-mail.<br>
						After that, you will be able to open the capsule at the specific capsule page (a button will appear).';*/

$lang['FULL_TUTORIAL_LINK'] = "Полная инструкция";
$lang['GOT_IT'] = "Понял!";

// Tutorial USER page
$lang['NEW_CAPSULE_BTN_TUTORIAL'] = "Нажмите на кнопку ниже, чтобы создать новую капсулу";
$lang['MY_CAPSULES_TUTORIAL'] = "Все капсулы будут находится в таблице ниже. Ручное удаление";

// Tutorial CAPSULE CREATION page
$lang['MESSAGE_TUTORIAL'] = "Введите сообщение для закрытия в графу ниже";
$lang['OPENING_DATE_TUTORIAL'] = "Выберите дату открытия капсулы ниже";
$lang['ENCLOSE_BTN_TUTORIAL'] = "Нажмите на кнопку ниже, чтобы закрыть капсулу";
				
// About page
$lang['FIND'] = 'Где нас найти?'; //'Where to find us?';
$lang['MK_DONATION'] = 'Сделать пожертвование';
$lang['CONTACT_INFO'] = 'Контактная информация';
$lang['REPO'] = 'Репозиторий';

// Bank
$lang['AMOUNT_EUR'] = 'Сумма (EUR)';
$lang['CONFIRM'] = 'Подтвердить';
$lang['DONATE'] = 'Пожертвовать';
$lang['DONATE_OK'] = 'Спасибо за пожертвование!';
$lang['DONATE_NOT_OK'] = 'Что-то пошло не так :( Платёж не был осуществлён.';
$lang['BACK_TO_MAIN'] = 'На главную';

?>