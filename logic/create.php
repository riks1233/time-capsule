<?php
session_start();
include_once 'db_connect.php';
include_once 'functions.php';

if(login_check($mysqli) != true) {
	header('Location: ../?error=3');
	exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_SESSION["username"])) {
	$username = $_SESSION["username"];
	$message = $_POST["message"];
	$datetime = $_POST["datetime"];
	
	// TODO: Checking if time format is correct.
	/*if () {
		header('Location: ../capsuleCreation?err_datetime=0');
		exit;
	}*/
	$datetime = $datetime . ":00";
	
	$stmt1 = $mysqli->prepare("CALL createCapsule(?, ?, ?, @output)");
	$stmt2 = $mysqli->prepare("SELECT @output as output");
	$stmt1->bind_param('sss', $message, $datetime, $username);
	
	$stmt1->execute();
	$stmt2->execute();
	
	$stmt2->bind_result($output);
	$stmt2->fetch();
	$stmt1->close();
	$stmt2->close();
	if ($output === 1) {
		$_SESSION["message"] = null;
		$_SESSION["datetime"] = null;
		header('Location: ../user.php');
	} else {
		$_SESSION["message"] = $_POST["message"];
		$_SESSION["datetime"] = $_POST["datetime"];
		header('Location: ../capsuleCreation.php?err_datetime=1');
	}
} else 
	header('Location: ../error.php?err=Invalid+request');
?>