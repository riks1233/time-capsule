<?php

function validateInput($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

function login($username, $password, $mysqli) {
	$password_hash = get_db_password_hash($mysqli, $username);//gets password hash from the database
	$auth = authorize($mysqli, $username, $password, $password_hash);
	if($auth == 1){
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
        $username = preg_replace("/[^a-zA-Z0-9_]+/", "", $username);
        $_SESSION['username'] = $username;
        $_SESSION['login_string'] = hash('sha512', $password_hash . $user_browser);
        // SUCCESS
    }
	return $auth;
}

function login_check($mysqli) {
    if (isset($_SESSION['username'], $_SESSION['login_string'])) {
        $username = $_SESSION['username'];
        $login_string = $_SESSION['login_string'];
 
        $user_browser = $_SERVER['HTTP_USER_AGENT'];//user-agent string
 
        $password_hash = get_db_password_hash($mysqli, $username);
        $login_check = hash('sha512', $password_hash . $user_browser);

        if (hash_equals($login_check, $login_string) ){
			//SUCCESS
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function to_hex($data){
	return strtoupper(bin2hex($data));
}

function decode($data, $kh){//kh võtta rsa.php'st
	$data = pack('H*', $data);
	if (openssl_private_decrypt($data, $r, $kh)) {
		return $r;
	} else {
		return null;
	}
}

function get_hash($password, $cost){
	return password_hash($password, PASSWORD_BCRYPT, ['cost' => $cost]);
}

function get_db_password_hash($mysqli, $username){
	$stmt1 = $mysqli->prepare("CALL getHash(?, @output)");
	//echo htmlspecialchars($mysqli->error);
	$stmt2 = $mysqli->prepare("SELECT @output as output");
	$stmt1->bind_param('s', $username);
		
	$stmt1->execute();
	$stmt2->execute();
		
	$stmt2->bind_result($output);
	$stmt2->fetch();
	
	$stmt1->close();
	$stmt2->close();
	sql_fix($mysqli);
	return $output;
}

function authorize($mysqli, $username, $password, $password_hash){
	if($password_hash == "not found")
		return 0;
	if(too_many_attempts($mysqli, $username))
		return 2;
	elseif(password_verify($password, $password_hash)){
		reset_failed_login($mysqli, $username);
		return 1;
	} else {
		count_failed_login($mysqli, $username);
		return 0;
	}
}

function too_many_attempts($mysqli, $username){
	$stmt1 = $mysqli->prepare("CALL tooManyAttempts(?, @output)");
	$stmt2 = $mysqli->prepare("SELECT @output as output");
	$stmt1->bind_param('s', $username);
		
	$stmt1->execute();
	$stmt2->execute();
	
	$stmt2->bind_result($output);
	$stmt2->fetch();
	
	$stmt1->close();
	$stmt2->close();
	sql_fix($mysqli);
	
	return $output;
}

function count_failed_login($mysqli, $username){
	$stmt1 = $mysqli->prepare("CALL countFailedLogin(?)");
	$stmt1->bind_param('s', $username);
		
	$stmt1->execute();
	
	$stmt1->close();
	sql_fix($mysqli);
}

function reset_failed_login($mysqli, $username){
	$stmt1 = $mysqli->prepare("CALL resetFailedLogin(?)");
	$stmt1->bind_param('s', $username);
		
	$stmt1->execute();
	
	$stmt1->close();
	sql_fix($mysqli);
}

function getName($mysqli, $username){
	$stmt1 = $mysqli->prepare("CALL getUserInfo(?)");
	$stmt1->bind_param('s', $username);
		
	$stmt1->execute();
	
	$stmt1->bind_result($name);
	$stmt1->fetch();//gets next row
	
	$stmt1->close();
	sql_fix($mysqli);
	if($name != "")
		return $name;
	else
		return $username;
}

function getCapsuleCount($mysqli, $username){
	$stmt1 = $mysqli->prepare("CALL getCapsuleCount(?)");
	$stmt1->bind_param('s', $username);
	$stmt1->execute();
	$stmt1->bind_result($count);
	$res = $stmt1->fetch();
	$stmt1->close();
	sql_fix($mysqli);//selleks et errori puhul ikka close() töötaks
	if($res)
		return $count;
	else
		return 0;
}

function deleteCapsule($mysqli, $id, $username){
	$stmt1 = $mysqli->prepare("CALL deleteCapsule(?, ?)");
	$stmt1->bind_param('si', $username, $id);
	$stmt1->execute();
	$stmt1->close();
	
	sql_fix($mysqli);
}

function getCapsules($mysqli, $username){
	$stmt1 = $mysqli->prepare("CALL getCapsules(?)");
	$stmt1->bind_param('s', $username);
		
	$stmt1->execute();
	
	$stmt1->bind_result($capsule_id, $capsule_message, $opening_time);
	while($stmt1->fetch())
		echo "<div class=\"capsuleField\">
				<a href=\"capsule.php?id=" . $capsule_id . "\">
					<div class=\"text capsuleFieldText\">" . $opening_time . "</div>
				</a>
				<div class=\"capsuleFieldRemove\">
					<form method=\"post\" action=\"deleteCapsule.php\">
						<input type=\"hidden\" name=\"id\" value=\"" . $capsule_id . "\">
						<button type=\"submit\" class=\"removeBtn\"><img src=\"images/X.png\" class=\"small_icon\" alt=\"delete\"/></button>
					</form>
				</div>
			</div>";
	$stmt1->close();
	sql_fix($mysqli);
}

function getCapsuleMessage($mysqli, $id, $username){
	$stmt1 = $mysqli->prepare("CALL getCapsuleMessage(?, ?, @output1)");
	$stmt2 = $mysqli->prepare("select @output1 as output1");
	$stmt1->bind_param('si', $username, $id);
    
	$stmt1->execute();
	$stmt2->execute();
    
	$stmt2->bind_result($message);
    $stmt2->fetch();
    
	$stmt1->close();
	$stmt2->close();
	sql_fix($mysqli);
    return $message;
}

function getCapsuleTime($mysqli, $id){
	$stmt1 = $mysqli->prepare("CALL getCapsuleTime(?, @output1, @output2)");
	$stmt2 = $mysqli->prepare("select @output1 as output1");
	$stmt3 = $mysqli->prepare("select @output2 as output1");
	$stmt1->bind_param('i', $id);

    $stmt1->execute();
	$stmt2->execute();
	$stmt3->execute();

    $stmt2->bind_result($time);
    $stmt3->bind_result($status);
    $stmt2->fetch();
    $stmt3->fetch();   

    $stmt1->close();
    $stmt2->close();
    $stmt3->close();
    sql_fix($mysqli);

    if($status == 0) return $time;
    else return null;

}

function process_login_result($login_result, $redirect, $form){
	if ($login_result == 1) {
        // Successesful login.
		if (isset($redirect)) {
			header('Location: ..' . $redirect);
		} else header('Location: ../user.php');
    } else {
		// Destroyed last session in the beginning, setting redirect again.
		if (isset($redirect)) {
			$_SESSION['REDIRECT'] = $redirect;
		}
		// Failed login. Back to the index with error.
		if ($form == "modal") {
			header('Location: ../?error=' . $login_result);
		} else header('Location: ../login.php?error=' . $login_result);
    }
}

function generate_random_string($length){
    if (function_exists('random_bytes'))
        return bin2hex(random_bytes($length));
    if (function_exists('mcrypt_create_iv'))
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    if (function_exists('openssl_random_pseudo_bytes'))
        return bin2hex(openssl_random_pseudo_bytes($length));
}

function sql_fix($mysqli){//sql errori puhul on vaja teha next_result, et edasi tegutseda
	if($mysqli->more_results())
		$mysqli->next_result();
}

?>