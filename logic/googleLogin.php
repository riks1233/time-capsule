<?php
include_once 'db_connect.php';
include_once 'functions.php';
session_start();

if (isset($_SESSION['REDIRECT'])) {
	$redirect = $_SESSION['REDIRECT'];
}
session_destroy();
sec_session_start();

include_once 'rsa.php';
include_once 'googleAPI.php';

$redirect_uri = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
$client->setRedirectUri($redirect_uri);

if(isset($_POST['id_token'], $_POST['email'], $_POST['name'])){
	$id_token = $_POST['id_token'];
	echo 'Issues on the Google side :(<br>Press F5 to retry.';
	usleep(500000);
	$payload = $client->verifyIdToken($id_token);
	if ($payload) {
	  $user_id = $payload['sub'];
	} else {
		die("Invalid ID token!");
	}
	
	$user_email = $_POST['email'];
	if (!filter_var($user_email, FILTER_VALIDATE_EMAIL))//turvalisus
		die("Invalid email!");
	$user_name = validateInput($_POST['name']);//turvalisus
	
	$username = 'google' . $user_id;
	
	$random_hash = bin2hex(generate_random_string(32));#uue kasutaja jaoks genereeritakse hash, mis pärast läheb login stringi hashi
	
	$stmt1 = $mysqli->prepare("CALL register(?, ?, ?, ?, @output, 1)");#teeb asju ainult esimesel loginil
	$stmt1->bind_param('ssss', $username, $user_email, $random_hash, $user_name);

	$stmt1->execute();
	$stmt1->close();
	
	$password_hash = get_db_password_hash($mysqli, $username);//saadakse esimesel loginil genereeritud hashi
	
	$user_browser = $_SERVER['HTTP_USER_AGENT'];
	$_SESSION['username'] = $username;
	$_SESSION['login_string'] = hash('sha512', $password_hash . $user_browser);
	process_login_result(1, $redirect, $form);
}
#header('Location: ../?error=' . $login_result);

?>