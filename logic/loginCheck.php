<?php 
session_start();
include_once 'logic/db_connect.php';
include_once 'logic/functions.php';

if(login_check($mysqli) != true) {
	$_SESSION['REDIRECT'] = $_SERVER['REQUEST_URI'];
	header('Location: ./?error=3');
	exit;
}
?>