<?php

// 'Restart' the ongoing session.
session_start();
 
// Overwrite all session values.
$_SESSION = array();
 
// Delete the session cookie. 
$params = session_get_cookie_params();
setcookie(session_name(),
        '', time() - 42000, 
        $params["path"], 
        $params["domain"], 
        $params["secure"], 
        $params["httponly"]);
 
// Destroy the session.
session_destroy();
header('Location: ../index.php');
exit;
?>