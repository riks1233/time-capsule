<?php
include_once 'db_connect.php';
session_start();
if (isset($_SESSION['REDIRECT'])) {
	$redirect = $_SESSION['REDIRECT'];
}
session_destroy();
sec_session_start();

include_once 'functions.php';
include_once 'rsa.php';

if (isset($_GET["form"])) {
	$form = validateInput($_GET["form"]);
}
 
if (isset($_POST['username'], $_POST['password'])) {
    $username = $_POST['username'];
    $password = $password_encrypted = $_POST['password'];
	
	if(strlen($password_encrypted) > 64 /*on kryptitud*/ && strlen($password_encrypted) < 1000 /*anti ddos*/ )
		$password = decode($password_encrypted, $kh);

	$login_result = login($username, $password, $mysqli);
    process_login_result($login_result, $redirect, $form);
	exit;
} else {
    // Invalid POST variables were sent to this page. 
    header('Location: ../error.php?err=Invalid Request');
	exit;
}
?>