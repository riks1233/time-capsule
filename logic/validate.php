<?php
include_once 'db_connect.php';
include_once 'functions.php';
include_once 'rsa.php';

if (isset($_GET["form"])) {
	$form = validateInput($_GET["form"]);
}
$reg_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$reg_username = validateInput($_POST["reg_username"]);
	if (!preg_match("/^[a-zA-Z0-9_]+$/",$reg_username)) {
		$reg_err = "err_username=0"; // Only letters, numbers and underscore allowed
	} 
	if (preg_match("/^google[0-9]+$/i",$reg_username)) {  // 
		$reg_err = "err_username=2"; // username with google and numbers is not allowed
	}

	$reg_encrypted_password = validateInput($_POST["reg_password"]);
	$reg_encrypted_pass_repeat = validateInput($_POST["reg_pass_repeat"]);
	$reg_password = $reg_encrypted_password;
	$reg_pass_repeat = $reg_encrypted_pass_repeat;
	if(strlen($reg_encrypted_password) > 64 /*on kryptitud*/ && strlen($reg_encrypted_password) < 1000 /*anti ddos*/ ){
		$reg_password = decode($reg_encrypted_password, $kh);
		$reg_pass_repeat = decode($reg_encrypted_pass_repeat, $kh);
	}
	if ($reg_password != $reg_pass_repeat) {
		if ($reg_err != "") { $reg_err = $reg_err . "&"; }
		$reg_err = $reg_err . "err_password=0"; // Passwords do not match
	}
	
	$reg_name = validateInput($_POST["reg_name"]);
	
	$reg_email = $_POST["reg_email"];
	$reg_email_repeat = $_POST["reg_email_repeat"];
		
	if ($reg_email != $reg_email_repeat) {
		if ($reg_err != "") { $reg_err = $reg_err . "&"; }
		$reg_err = $reg_err . "err_email=0"; // Emails do not match
	} else {
		if (!filter_var($reg_email, FILTER_VALIDATE_EMAIL)) {
			if ($reg_err != "") { $reg_err = $reg_err . "&"; }
			$reg_err = $reg_err . "err_email=1"; // Invalid email format
		}
	}
	
	$to_remove = array("&", "=", "?");//just in case
	$saved_username = str_replace($to_remove, "", $reg_username);
	$saved_name = str_replace($to_remove, "", $reg_name);
	$saved_email = str_replace($to_remove, "", $reg_email);
	$saved_email_repeat = str_replace($to_remove, "", $reg_email_repeat);
	$saved_form = "&" . "reg_username=" . $saved_username . "&" . "reg_name=" . $saved_name . "&" . "reg_email=" . $saved_email . "&" . "reg_email_repeat=" . $saved_email_repeat;
		
	if ($reg_err == "") {
		$password_hash = get_hash($reg_password, 11);
		//echo $password_hash . "<br>";
		$stmt1 = $mysqli->prepare("CALL register(?, ?, ?, ?, @output, 0)");
		$stmt2 = $mysqli->prepare("SELECT @output as output");
		$stmt1->bind_param('ssss', $reg_username, $reg_email, $password_hash, $reg_name);

		$stmt1->execute();
		$stmt2->execute();
	
		$stmt2->bind_result($output);
		$stmt2->fetch();
		$stmt1->close();
		$stmt2->close();
		if ($output == 0){
			//echo "SUCCESSFULLY REGISTERED <br>";
			if(authorize($mysqli, $reg_username, $reg_password, $password_hash))
				header('Location: ../index.php?success=1'); // echo "authorization test passed <br>";
			else
				header('Location: ../?auth_err=0'); // echo "authorization test failed <br>";
		} elseif ($output == 1) { // Username already in use
			if ($form == "modal") {
				header('Location: ../?err_username=1' . $saved_form); 
			} else 
				header('Location: ../register.php?err_username=1' . $saved_form);
		} else { // Email already in use
			if ($form == "modal") {
				header('Location: ../?err_email=2' . $saved_form);
			} else 
				header('Location: ../register.php?err_email=2' . $saved_form);
		}
	} else
		if ($form == "modal") {
			header('Location: ../?' . $reg_err . $saved_form);
		} else 
			header('Location: ../register.php?' . $reg_err . $saved_form);
} else 
	header('Location: ../error.php?err=Invalid+request');
?>