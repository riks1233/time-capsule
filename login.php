<?php 
session_start();
include_once 'logic/db_connect.php';
include_once 'logic/functions.php';
include_once 'config/google-config.php';

if(login_check($mysqli) == true) {
	header('Location: ./user.php');
	exit;
}
include_once 'lang/language.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head>
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_LOGIN'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<!-- Google API -->
		<meta name="google-signin-client_id" content="<?=CLIENT_ID?>">
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<!-- End of Google API-->
		<script async src ="downloaded/jquery/jquery-3.1.1.min.js"></script>
		<script async src="scripts/googleAPI.js"></script>
	</head>
	<body>
	<?php 
		include_once './logic/rsa.php';
	?>
	
	<div id="mainContainer">
		<div id="pageContent">
			<p><?= getLocalised('LOGIN'); ?></p>
			<form id = "login_form" action="logic/process_login.php?form=page" method="post">
				<input title="<?= getLocalised('USERNAME'); ?>" name="username" class="inputField" type="text" maxlength="25"  placeholder="<?= getLocalised('USERNAME'); ?>" required>
				<div class="errorField"><span id="login-error"></span></div>
				<input title="<?= getLocalised('PASSWORD'); ?>" name="password" class="inputField" type="password" maxlength="30"  placeholder="<?= getLocalised('PASSWORD'); ?>" required>
				<div class="errorField">
					<?php 
						if (isset($_GET['error'])){
							$err = $_GET['error'];
							if($err == 0)
								echo getLocalised('LOGIN_ERR_NULL');
							elseif ($err == 2)
								echo getLocalised('LOGIN_ERR_TWO');
							elseif ($err == 3)
								echo getLocalised('LOGIN_ERR_THREE');
						} 
					?>
				</div>
				<p>
					<input type="submit" value="<?= getLocalised('LOGIN_BTN'); ?>" class="small_button">
				</p>
				<a href="https://id.ajakapsel.me/idlogin.php"><img src="images/IDcard.gif" alt="<?= getLocalised('ID_LOGIN'); ?>"/></a>
				<?= getLocalised('OR') ?>
				<div class="g-signin2" data-onsuccess="onSignIn"></div>
			</form>
			<script src="scripts/rsa/rsaAsync.js"></script>
			<br><br>
			<a href="index.php" class="howto"><?= getLocalised('BACK_TO_MAIN'); ?></a>
		</div>
	</div>
	
	</body>

</html>