<?php 
session_start();
include_once 'logic/db_connect.php';
include_once 'logic/functions.php';

if(login_check($mysqli) == true) {
	header('Location: ./user.php');
	exit;
}
include_once 'lang/language.php';
?>
<!DOCTYPE html>
<html lang="<?= getLocalised('LANG'); ?>">
	<head> 
		<meta charset="UTF-8">
		<title><?= getLocalised('TITLE_REGISTER'); ?></title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<script src ="downloaded/jquery/jquery-3.1.1.min.js"></script>
	</head>
	<body>
	<?php 
		include_once './logic/rsa.php';
	?>
	
	<div id="mainContainer">
		<div id="pageContent">
			<p><?= getLocalised('REGISTER'); ?></p>
			<form id = "register_form" action="logic/validate.php?form=page" method="post">
				<input title="<?= getLocalised('USERNAME'); ?>" name="reg_username" class="inputField" type="text" placeholder="<?= getLocalised('USERNAME'); ?>" maxlength="25" minlength="5" required value="<?php if(isset($_GET["reg_username"]))echo $_GET["reg_username"]?>">
				<div class="errorField">
					<?php 
						if (isset($_GET["err_username"])) {
							$err_username = $_GET["err_username"];
							if ($err_username == 0)
								echo getLocalised('REGISTER_ERR_USER_NULL');
							elseif ($err_username == 1)
								echo getLocalised('REGISTER_ERR_USER_ONE');
							elseif ($err_username == 2)
								echo getLocalised('REGISTER_ERR_USER_TWO');
						} 
					?>
				</div>
				<input title="<?= getLocalised('PASSWORD'); ?>" name="reg_password" class="inputField" type="password" placeholder="<?= getLocalised('PASSWORD'); ?>" maxlength="30" minlength="8" required>
				<div class="errorField"></div>
				<input title="<?= getLocalised('REGISTER_PASS_REPEAT'); ?>" name="reg_pass_repeat" class="inputField" type="password" placeholder="<?= getLocalised('REGISTER_PASS_REPEAT'); ?>" maxlength="30" minlength="8" required>
				<div class="errorField">
					<?php 
						if (isset($_GET["err_password"])) {
							$err_password = $_GET["err_password"];
							if ($err_password == 0)
								echo getLocalised('REGISTER_ERR_PASS');
						} 
					?>
				</div>
				<input title="<?= getLocalised('REGISTER_NAME'); ?>" name="reg_name" class="inputField" type="text" placeholder="<?= getLocalised('REGISTER_NAME'); ?>" maxlength="50"  value="<?php if(isset($_GET["reg_name"]))echo $_GET["reg_name"]?>">
				<div class="errorField"></div>
				<input title="<?= getLocalised('REGISTER_EMAIL'); ?>" name="reg_email" class="inputField" type="email" placeholder="<?= getLocalised('REGISTER_EMAIL'); ?>" maxlength="50" required value="<?php if(isset($_GET["reg_email"]))echo $_GET["reg_email"]?>">
				<div class="errorField"></div>
				<input title="<?= getLocalised('REGISTER_EMAIL_REPEAT'); ?>" name="reg_email_repeat" class="inputField" type="email" placeholder="<?= getLocalised('REGISTER_EMAIL_REPEAT'); ?>" maxlength="50" required value="<?php if(isset($_GET["reg_email_repeat"]))echo $_GET["reg_email_repeat"]?>">
				<div class="errorField">
					<?php 
						if (isset($_GET["err_email"])) {
							$err_email = $_GET["err_email"];
							if ($err_email == 0)
								echo getLocalised('REGISTER_ERR_NULL');
							elseif ($err_email == 1)
								echo getLocalised('REGISTER_ERR_ONE');
							elseif ($err_email == 2)
								echo getLocalised('REGISTER_ERR_TWO');
						} 
					?>
				</div>
				<p>
					<input type="submit" value="<?= getLocalised('REGISTER_BTN'); ?>" class="small_button">
				</p>
			</form>
			<script src="scripts/rsa/rsaAsync.js"></script>
			<br>
			<a href="index.php" class="howto"><?= getLocalised('BACK_TO_MAIN'); ?></a>
		</div>
	</div>
	
	</body>

</html>