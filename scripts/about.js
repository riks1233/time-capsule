var xml;
var xsl1;
var xsl2;
var loaded = 0;

function onLoad1(xmlhttp){
	xml = getDocFromHttp(xmlhttp);
	countLoaded(xmlhttp);
}
function onLoad2(xmlhttp){
	xsl1 = getDocFromHttp(xmlhttp);
	countLoaded(xmlhttp);
}
function onLoad3(xmlhttp){
	xsl2 = getDocFromHttp(xmlhttp);
	countLoaded(xmlhttp);
}

loadXML("/xml/about.xml", onLoad1/*IE*/,
function(){
	if(this.readyState == 4 && this.status == 200){
		console.log(this.responseXML);
		onLoad1(this);
	}
});

loadXML("/xslt/about1.xsl", onLoad2/*IE*/,
function(){
	if(this.readyState == 4 && this.status == 200){
		console.log(this.responseXML);
		onLoad2(this);
	}
});

loadXML("/xslt/about2.xsl", onLoad3/*IE*/, 
function(){
	if(this.readyState == 4 && this.status == 200){
		console.log(this.responseXML);
		onLoad3(this);
	}
});

function countLoaded(xmlhttp){
	loaded++;
	if(loaded == 3){
		transformAndAppendToElement("contact_info", xml, xsl1);
		transformAndAppendToElement("repo_link", xml, xsl2);
	}
}