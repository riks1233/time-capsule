function post(path, params, method) {
	/*
		Credit: http://stackoverflow.com/questions/133925/javascript-post-request-like-a-form-submit
	*/
    method = method || "post";
	
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  //console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  //console.log('Name: ' + profile.getName());
  //console.log('Image URL: ' + profile.getImageUrl());
  //console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
  var id_token = googleUser.getAuthResponse().id_token;
  post("logic/googleLogin.php", { id_token:id_token, email:profile.getEmail(), name:profile.getName() });
}

function googleLogout(){
	//alert("dafuq2");
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
	//alert("dafuq3");
}

function onLoad() {
	gapi.load('auth2', function() {
		gapi.auth2.init();
	});
}

$("#logout_button").submit(function(event){
	//alert("dafuq");
	googleLogout();
});