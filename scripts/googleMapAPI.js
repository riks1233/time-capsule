function myMap() {
	var mapProp = {
		center:new google.maps.LatLng(58.3782,26.715),
		zoom:18
	};
	var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
	var marker = new google.maps.Marker({ 
		map: map, position: new google.maps.LatLng(58.378161, 26.714728) 
	});
	var infowindow = new google.maps.InfoWindow({
		content: '<strong>Time Capsule</strong><br>Juhan Liivi 2, 50409 Tartu, Estonia'
	});
	google.maps.event.addListener(marker, 'click', function() { 
		infowindow.open(map, marker); 
	});
};