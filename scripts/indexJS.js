var cdnAvailable = true;

/**
 * Get modal form that is meant to be displayed.
 * Displays modal with style "block".
 */

function loginBtnFunc() {
	var loginModal = document.getElementById('loginModal');
	loginModal.style.display = "block";
}

function registerBtnFunc() {
	var registerModal = document.getElementById('registerModal');
	registerModal.style.display = "block";
}

/**
 * Get modal form that was displayed.
 * Hide modal by changing its display style to "none".
 */
 
function loginSpanClose() {
	var loginModal = document.getElementById('loginModal');
	loginModal.style.display = "none";
}

function registerSpanClose() {
	var registerModal = document.getElementById('registerModal');
	registerModal.style.display = "none";
}

/**
 * Draws normal buttons by "writing" this (HTML) code below.
 */

function drawNormalButtons(prompt1, prompt2, prompt3, prompt4, login, register){
	document.getElementById('normalButtons').innerHTML = "<p>" + prompt1 + " " +
					"<a onclick=\"loginBtnFunc()\">" + prompt2 + " " + "</a>" + prompt3 + " " + 
					"<a onclick=\"registerBtnFunc()\">" + prompt4 + "</a>" +
				"</p>" +
				"<p>" +
					"<div id=\"login_buttons\"><button id=\"loginButton\"  class=\"small_button\" onclick=\"loginBtnFunc()\">" + login + "</button></div>" +
				"</p>" +
				"<p>" +
					"<button id=\"registerButton\" class=\"small_button\" onclick=\"registerBtnFunc()\">" + register + "</button>" +
				"</p>";
}

drawNormalButtons(prompt1, prompt2, prompt3, prompt4, loginButtonText, registerButtonText);//variables defined via an inlined script and php

if(typeof onIndexLoad != 'undefined'){
	if(onIndexLoad == 0)
		loginBtnFunc()
	else if(onIndexLoad == 1)
		registerBtnFunc()
}