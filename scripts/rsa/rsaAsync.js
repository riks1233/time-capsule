$.getScript('downloaded/jsbn/jsbn.js').done(function( script, textStatus ) {
	$.getScript('downloaded/jsbn/prng4.js').done(function( script, textStatus ) {
		$.getScript('downloaded/jsbn/rng.js').done(function( script, textStatus ) {
			$.getScript('downloaded/jsbn/rsa.js').done(function( script, textStatus ) {
				console.log("rsa js loaded");
				$("#register_form").submit(function(event){
					var form = $("#register_form");
					var field1 = form.find('input[name="reg_password"]');
					var field2 = form.find('input[name="reg_pass_repeat"]');
					if(field1.val() == field2.val()){
						var encrypted1 = encryptWithRsa(field1.val());
						var encrypted2 = encryptWithRsa(field2.val());
						field1.val(encrypted1);
						field2.val(encrypted2);
					} else {//perfomance booster
						field1.val("password1");
						field2.val("password2");
					}
				});

				$("#login_form").submit(function(event){
					var form = $("#login_form");
					var password_field = form.find('input[name="password"]');
					var encrypted = encryptWithRsa(password_field.val());
					password_field.val(encrypted);
				});
			});
		});
	});
});

function encryptWithRsa(s){
	var rsa = new RSAKey();
	rsa.setPublic(public_key, public_key_e);//sent from the server by rsa.php
	var enc = rsa.encrypt(s);
	return enc;
}

