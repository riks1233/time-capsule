
function timer(openingDate){
	// Get countDownDate from database, delete the line below
	//var openingDate = new Date("Feb 10, 2019 7:39:25").getTime();
	var start = new Date().getTime();
	
	// Update the count down every 1 second
	var x = setInterval(function() {
		
		// Get current datetime
		var now = new Date().getTime();

		// Find the distance between now an the count down date
		var distance = openingDate + start - now;
		
		if(distance < 0)
			distance = 0;

		// Time calculations for days, hours, minutes and seconds
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)).toString();
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)).toString();
		var seconds = Math.floor((distance % (1000 * 60)) / 1000).toString();

		// make hours, minutes, seconds always two-digit
		if (hours.length == 1){
			hours = "0" + hours;
		}
		if (minutes.length == 1){
			minutes = "0" + minutes;
		}
		if (seconds.length == 1){
			seconds = "0" + seconds;
		}

		//Put time into span with id="time"
		document.getElementById("timeText").innerHTML = days + "d " + hours + ":"
		+ minutes + ":" + seconds;
	}, 1000);
}