

function showMessageTutorial(){
	var message_tutorial = document.getElementById('message_tutorial');
	var positionY = get_y_of("capsuleCreationText");
	message_tutorial.style.paddingTop = positionY + "px";
	message_tutorial.style.display = "block";
}

function showDateTutorial(){
	/*close prev modal*/
	var message_tutorial = document.getElementById('message_tutorial');
	message_tutorial.style.display = "none";
	
	var date_tutorial = document.getElementById('date_tutorial');
	var positionY = get_y_of("dateTime");
	date_tutorial.style.paddingTop = positionY + "px";
	date_tutorial.style.display = "block";
}

function showEncloseBtnTutorial(){
	/*close prev modal*/
	var date_tutorial = document.getElementById('date_tutorial');
	date_tutorial.style.display = "none";
	
	var enclosebtn_tutorial = document.getElementById('enclosebtn_tutorial');
	var positionY = get_y_of("encloseBtn");
	enclosebtn_tutorial.style.paddingTop = positionY + "px";
	enclosebtn_tutorial.style.display = "block";
}

function closeTutorial(){
	var enclosebtn_tutorial = document.getElementById('enclosebtn_tutorial');
	enclosebtn_tutorial.style.display = "none";
}

function drawNormalHelp(helpText){
	document.write(
	"<p><a class=\"howto\" onclick=\"showMessageTutorial()\">" + helpText + "</a></p>")
}

function get_y_of(elementID){
	var Element = document.getElementById(elementID);
	var rect = Element.getBoundingClientRect();
	return rect.top - 160;
}