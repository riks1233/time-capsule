

function showNewCapsuleBtnTutorial(){
	var newcapsulebtn_tutorial = document.getElementById('newcapsulebtn_tutorial');
	var positionY = get_y_of("newCapsuleBtn");
	newcapsulebtn_tutorial.style.paddingTop = positionY + "px";
	newcapsulebtn_tutorial.style.display = "block";
}

function showMyCapsulesTutorial(){
	/*close prev modal*/
	var newcapsulebtn_tutorial = document.getElementById('newcapsulebtn_tutorial');
	newcapsulebtn_tutorial.style.display = "none";
	
	var mycapsules_tutorial = document.getElementById('mycapsules_tutorial');
	var positionY = get_y_of("myCapsules");
	mycapsules_tutorial.style.paddingTop = positionY + "px";
	mycapsules_tutorial.style.display = "block";
}

function closeTutorial(){
	var mycapsules_tutorial = document.getElementById('mycapsules_tutorial');
	mycapsules_tutorial.style.display = "none";
}

function drawNormalHelp(helpText){
	document.write(
	"<p><a class=\"howto\" onclick=\"showNewCapsuleBtnTutorial()\">" + helpText + "</a></p>")
}

function get_y_of(elementID){
	var Element = document.getElementById(elementID);
	var rect = Element.getBoundingClientRect();
	return rect.top - 160;
}