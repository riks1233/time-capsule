var isIE = isInternetExplorer();

function loadXML(url, onLoadIE, onStateChange){
	if (isIE){//IE
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
		xmlhttp.open("GET", url, false);
		if (xmlhttp.overrideMimeType) 
			xmlhttp.overrideMimeType('text/xml');  
		xmlhttp.send(null);
		onLoadIE(xmlhttp);
	} else {//other
		xmlhttp = new XMLHttpRequest();
		//xmlhttp.onLoad = onLoad;//custom function
		xmlhttp.open("GET", url, true);
		xmlhttp.setRequestHeader('Content-Type',  'text/xml');
		if (xmlhttp.overrideMimeType) 
			xmlhttp.overrideMimeType('text/xml');  
		xmlhttp.onreadystatechange = onStateChange;
		xmlhttp.send();
	}
	return xmlhttp;
}

function transformAndAppendToElement(elementId, xml, xsl){
	if (isIE){//IE
		document.getElementById(elementId).innerHTML = xml.transformNode(xsl);
	}else 
		document.getElementById(elementId).appendChild(transformXMLWithXSL(xml, xsl));
}

function transformXMLWithXSL(xml, xsl){
	xsltProcessor = new XSLTProcessor();
	xsltProcessor.importStylesheet(xsl);
	resultDocument = xsltProcessor.transformToFragment(xml, document);
	return resultDocument;
}

function isInternetExplorer(){
	var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
		return true;
	return false;
}

function getDocFromHttp(xmlhttp){
	var xmlDoc = xmlhttp.responseXML;
    if (!xmlDoc) {
		if (isIE) {
			xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
			xmlDoc.async = false;
			xmlDoc.loadXML(xmlhttp.responseText);
		} else
			xmlDoc = (new DOMParser()).parseFromString(xmlhttp.responseText, 'text/xml');     
	}
	return xmlDoc;
}