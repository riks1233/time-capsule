----------------------------------------------------------------------
Execute autotest.bat to run tests.
	Output in folder report, file googlechrome.results.html
----------------------------------------------------------------------

HTML Tests generated with Selenium IDE.

PS! Tests are designed to use on real page: https://www.ajakapsel.me
	using Google Chrome browser
	and cooldown for test "login_toomanyattempts_needs_cooldown" is 15 minutes.

selenium-html-runner-3.4.0.jar from: http://www.seleniumhq.org/download/
chromedriver.exe from: https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver

Dependencies:
Google Chrome
Java: 1.5 or newer
OS: Windows

Tested with:
Google Chrome: Version 57.0.2987.133 (64-bit)
Java: 1.8.0_121
OS: Windows 8.1 Enterprise 64-bit
