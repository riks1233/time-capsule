<xsl:transform version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
    <xsl:for-each select="ABOUT/TEAM/MEMBER">
		<p><xsl:value-of select="NAME"/>: <xsl:value-of select="EMAIL"/></p>
    </xsl:for-each>
  </body>
  </html>
</xsl:template>

</xsl:transform>