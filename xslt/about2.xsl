<xsl:transform version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
    <xsl:for-each select="ABOUT">
		<p>Bitbucket: <a href="{REPO}" target="blank"><xsl:value-of select="REPO"/></a></p>
    </xsl:for-each>
  </body>
  </html>
</xsl:template>

</xsl:transform>